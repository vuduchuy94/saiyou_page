import React from 'react';
import logo from './logo.svg';
import './App.css';
import GeneralRecruitPage from './pages/GeneralRecruitPage';

function App() {
  return (
    <div className="App">
			<GeneralRecruitPage />
    </div>
  );
}

export default App;
