// const RecruitJSON = {
//     pageInfo: {
//       title: "World Recruitment",
//       description: ""
//     },
  
//     sections: [
//       {
//         type: "TextList",
//         heading: "REASON",
//         subHeading: "Lý do để bạn lựa chọn WORLD",
//         content:
//           "Bạn là kỹ sư IT trẻ trung, năng động và muốn làm việc lâu dài tại Nhật Bản, còn chần chờ gì nữa mà không ứng tuyển ngay vào World.",
//         items: [
//           {
//             image: { src: "avatar-1.png", alt: "ly do" },
//             title: "Cơ hội làm việc",
//             text:
//               "Bạn là sinh viên vừa tốt nghiệp và mong muốn được tìm được việc làm sau khi ra trường. Hay bạn là nhân viên IT muốn tìm công việc mới phù hợp chuyên môn và chế độ tốt hơn. World có rất nhiều cơ hội việc làm trên nhiều lĩnh vực để bạn thỏa mãn đam mê sáng tạo và gặt hái thành công. "
//           },
//           {
//             image: { src: "avatar-2.png", alt: "ly do" },
//             title: "Cơ hội học tập",
//             text:
//               "World mang đến cho bạn cơ hội thử thách trong công việc  và chuyên môn không ngừng thay đổi. Bạn không chỉ được học các kỹ thuật mới từ các chuyên gia IT Nhật Bản mà còn được trao dồi thêm năng lực Nhật Ngữ và khả năng giao tiếp, khả năng quản lí dự án.."
//           }
//         ],
//         images: { src: "teamwork.jpg", alt: "team" },
//         paragraphs: [
//           {
//             image: { src: "avatar-3.png", alt: "ly do" },
//             title: "Cơ hội thăng tiến",
//             text:
//               "World đang hợp tác với các khách hàng lớn, tiềm năng  trên toàn quốc, bạn sẽ có nhiều cơ hội được làm việc và trở thành Keymember tại các văn phòng của khách hàng World"
//           },
//           {
//             image: { src: "avatar-4.png", alt: " ly do" },
//             title: "Cơ hội giao lưu",
//             text:
//               "World là công ty global nơi hội tụ nhiều nhân tài từ nhiều quốc gia trên thế giới. Vì vậy ngoài làm việc bạn có thể học hỏi giao lưu nhiều văn hóa khác nhau qua các hoạt động ngoại khóa."
//           }
//         ]
//       }
  
      
//     ]
//   };
  
//   export default RecruitJSON;
  