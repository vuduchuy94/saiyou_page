
import React from 'react';
import Header from '../blocks/header-block';
import Reason from '../blocks/reason-block';
import Impression from '../blocks/Impression';
import JoinOwrTeam from '../blocks/JoinOwrTeam';

const GeneralRecruitPage =  () => {
	return (
		<React.Fragment>
			<Header />
			<Reason />
			<Impression />
			<JoinOwrTeam />
		
		</React.Fragment>
	);
}

export default GeneralRecruitPage;
